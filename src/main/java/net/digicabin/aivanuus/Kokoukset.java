package net.digicabin.aivanuus;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.*;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ListView;
import android.widget.Toast;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.DynamicLayout;
import android.view.View;
import android.widget.Button;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Kokoukset extends Activity {
    public static ArrayList<String> meetings;
    public String daytoactivity;
    public String part;
    private Button button;
    public static ArrayList<String> ryhmikset;
    public static List<String> listDataHeader;
    HashMap<String, List<String>> listDataChild;
    String url = "https://www.nasuomi.org/wp-json/wp/v2/kokoukset?per_page=100&page=1";
    String secondurl = "https://www.nasuomi.org/wp-json/wp/v2/kokoukset?per_page=100&page=2";
    public static List<String> thisday = new ArrayList<String>();
    static ArrayList meetingsfive = new ArrayList();


    static ArrayList items = new ArrayList();
    private ListView listview;
    private ArrayAdapter adapter;
    Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kokoukset);

        prepareListData(url);

        super.onResume();

        startTimer();

        Toast.makeText(Kokoukset.this,
                "Odota pieni hetki, kokoustietoja haetaan!", Toast.LENGTH_LONG).show();



        Intent extras = getIntent();
        part = extras.getStringExtra("part");
        daytoactivity = extras.getStringExtra("daytoactivity");


        final ListView listview = (ListView) findViewById(R.id.listView);

        Timer timer;
        TimerTask timerTask;




       adapter = new ArrayAdapter<String>(Kokoukset.this, android.R.layout.simple_list_item_1, items);



        listview.setAdapter(adapter);

        Kokoukset.this.adapter.notifyDataSetChanged();






        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String kaljaa = (String) meetingsfive.get(position);
                oneMeeting(kaljaa);
            }
        });







        button = (Button) findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                items.clear();
                thisday.clear();
                openMain2Activity(daytoactivity);

            }
        });




    }





    public void openMain2Activity(String daytoactivity) {


        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("daytoactivity", daytoactivity);


        startActivity(intent);


    }
    public void oneMeeting(String nameofmeeting) {


        Intent intent = new Intent(this, Onemeeting.class);
        intent.putExtra("nameofmeeting", nameofmeeting);
        intent.putExtra("daytoactivity", daytoactivity);
        intent.putExtra("part", part);


        startActivity(intent);


    }

    public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask, 1000, 1000); //
    }
    public void stoptimertask(View v) {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        //get the current timeStamp



                     Kokoukset.this.adapter.notifyDataSetChanged();







                    }
                });
            }
        };
    }



    private void prepareListData(String url) {
        this.url = url;
        this.meetings = meetings;

        kakkaa(url);
        kakkaa(secondurl);



    }



    public void kakkaa(String url) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Call response = client.newCall(request);

        response.enqueue(new Callback() {


            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String json = response.body().string();
                getSomeJson getting = new getSomeJson(json, daytoactivity, part);
                Kokoukset.meetings = getting.gettingArray();





                int i = 0;

                while (i < meetings.size()) {

                    thisday.add(meetings.get(i));






                    i ++;
                }




                }

        });
    }



}