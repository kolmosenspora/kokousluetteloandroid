package net.digicabin.aivanuus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button monday;
    private Button tuesday;
    private Button wednesday;
    private Button thursday;
    private Button friday;
    private Button saturday;
    private Button sunday;
    private Button search;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        monday = (Button) findViewById(R.id.monday);
        tuesday = (Button) findViewById(R.id.tuesday);
        wednesday = (Button) findViewById(R.id.wednesday);
        thursday = (Button) findViewById(R.id.thursday);
        friday = (Button) findViewById(R.id.friday);
        saturday = (Button) findViewById(R.id.saturday);
        sunday = (Button) findViewById(R.id.sunday);
        search = (Button) findViewById(R.id.search);


        monday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "monday";
                openMain2Activity(day);
            }
        });

        tuesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "tuesday";
                openMain2Activity(day);
            }
        });

        wednesday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "wednesday";
                openMain2Activity(day);
            }


        });

        thursday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "thursday";
                openMain2Activity(day);
            }
        });
        friday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "friday";
                openMain2Activity(day);
            }
        });
        saturday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "saturday";
                openMain2Activity(day);
            }
        });
        sunday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "sunday";
                openMain2Activity(day);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String day = "mondaytuesdaywednesdaythursdayfridaysaturdaysunday";
                openMain2Activity(day);
            }
        });



    }
    public void openMain2Activity(String day) {


        Intent intent = new Intent(this, Main2Activity.class);
        intent.putExtra("daytoactivity", day);



        startActivity(intent);





    }
}
