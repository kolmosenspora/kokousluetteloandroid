package net.digicabin.aivanuus;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.BreakIterator;

public class Main2Activity extends AppCompatActivity {
    public static TextView textView;
    String day = "null";
    private String daytoactivity;
    private Button back;
    private Button middle;
    private Button etela;
    private Button keski;
    private Button ita;
    private Button lansi;
    private Button pohjoinen;



    public Main2Activity() {

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        textView = (TextView) findViewById(R.id.textView);
        back = (Button) findViewById(R.id.back);

        middle = (Button) findViewById(R.id.middle);

        etela = (Button) findViewById(R.id.etela);
        pohjoinen = (Button) findViewById(R.id.pohjoinen);
        ita = (Button) findViewById(R.id.ita);
        lansi = (Button) findViewById(R.id.lansi);
        keski = (Button) findViewById(R.id.keski);


        Intent extras = getIntent();
        daytoactivity = extras.getStringExtra("daytoactivity");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (daytoactivity.equals("monday")) {
                    textView.setText("Maanantai");
                }
                if (daytoactivity.equals("tuesday")) {
                    textView.setText("Tiistai");
                }
                if (daytoactivity.equals("wednesday")) {
                    textView.setText("Keskiviikko");
                }
                if (daytoactivity.equals("thursday")) {
                    textView.setText("Torstai");
                }
                if (daytoactivity.equals("friday")) {
                    textView.setText("Perjantai");
                }
                if (daytoactivity.equals("saturday")) {
                    textView.setText("Lauantai");
                }
                if (daytoactivity.equals("sunday")) {
                    textView.setText("Sunnuntai");
                }
                if (daytoactivity.equals("mondaytuesdaywednesdaythursdayfridaysaturdaysunday")){
                    textView.setText("Kaikki päivät");
                }
            }
        });



        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMainActivity();

            }
        });



        etela.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String part = "etela";
                openActivity_kokoukset(part, daytoactivity);
            }
        });
        ita.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String part = "ita";
                openActivity_kokoukset(part, daytoactivity);
            }
        });
        lansi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String part = "lansi";
                openActivity_kokoukset(part, daytoactivity);
            }
        });
        pohjoinen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String part = "pohjoinen";
                openActivity_kokoukset(part, daytoactivity);
            }
        });
        keski.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String part = "keski";
                openActivity_kokoukset(part, daytoactivity);
            }
        });

        final Spinner dropdown = findViewById(R.id.spinner1);
        String[] items = new String[]{"Valitse kaupunki", "Helsinki", "Tampere", "Joensuu", "Turku", "Oulu", "Kerava", "Pori", "Jyväskylä", "Rovaniemi", "Karhunpää", "Iisalmi", "Lappeenranta", "Mikkeli", "Tohmajärvi", "Kotka", "Pieksämäki", "Kontiolahti", "Savonlinna", "Varkaus", "Lieksa", "Parikkala", "Paihola", "Hämeenlinna", "Karkkila", "Lahti", "Porvoo", "Espoo", "Hyvinkää", "Järvenpää", "Nummela", "Lohja", "Vantaa", "Tervalampi", "Forssa", "Lammi", "Riihimäki", "Nukari", "Hanko", "Kaarina", "Vaasa", "Karjaa", "Seinäjoki", "Lapua", "Malax", "Mariehamn", "Eura", "Köyliö", "Raisio", "Salo", "Rauma", "Kankaanpää", "Keitelepohja", "Ylöjärvi"  };
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        dropdown.setAdapter(adapter);

        Button something = findViewById(R.id.somethingelse);

        something.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (dropdown.getSelectedItem() == "Valitse") {

                    Toast.makeText(Main2Activity.this,
                            "Valitse jokin kaupunki!", Toast.LENGTH_LONG).show();
                }



                else {
                    String part = dropdown.getSelectedItem().toString();
                    openActivity_kokoukset(part, daytoactivity);
                }
            }
        });





    }

    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);




        startActivity(intent);
    }

    public void openActivity_kokoukset(String part, String daytoactivity) {


        Intent intent = new Intent(this, Kokoukset.class);
        intent.putExtra("daytoactivity", daytoactivity);
        intent.putExtra("part", part);

        startActivity(intent);
    }


}
