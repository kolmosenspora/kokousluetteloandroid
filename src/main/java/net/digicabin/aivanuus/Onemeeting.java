package net.digicabin.aivanuus;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Onemeeting extends AppCompatActivity {

    public String nameofmeeting;
    private TextView name;
    String url = "https://www.nasuomi.org/wp-json/wp/v2/kokoukset?per_page=100&page=1";
    String secondurl = "https://www.nasuomi.org/wp-json/wp/v2/kokoukset?per_page=100&page=2";
    public String part;
    public String daytoactivity;
    public ArrayList jee;
    public static TextView textView;
    Button button2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onemeeting);

        button2 = (Button) findViewById(R.id.button2);
        textView = (TextView) findViewById(R.id.textView2);
        Intent extras = getIntent();
        nameofmeeting = extras.getStringExtra("nameofmeeting");
        daytoactivity = extras.getStringExtra("daytoacticity");
        part = extras.getStringExtra("part");

        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Kokouksetactivity(nameofmeeting, daytoactivity, part);
            }
        });


      // This class is under construction!

        if (getSomeJson.trying.containsKey(nameofmeeting)) {


            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Onemeeting.this.textView.setText(getSomeJson.trying.get(nameofmeeting).toString());
                }
            });



        }






    }
    public void Kokouksetactivity(String nameofmeeting, String daytoactivity, String part) {


        Intent intent = new Intent(this, Kokoukset.class);
        intent.putExtra("nameofmeeting", nameofmeeting);
        intent.putExtra("daytoactivity", daytoactivity);
        intent.putExtra("part", part);


        startActivity(intent);


    }
}

